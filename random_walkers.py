#!/usr/bin/env python
# coding: utf-8

# # library

# In[1]:


import numpy as np
import math
import matplotlib.pyplot as plt
import turtle
from operator import add


# # Input

# In[2]:


number_random_walker = int (input("number_random_walker?  "))
number_step = int (input("number_step?  "))
length_step = float(input("length_step?  "))


# # Random walk function

# In[6]:


def random_walkers ( number_random_walker =100
                    , number_step = 100
                    , length_step = 1 
                    , p_forward = 0.5 
                    , p_backward = 0.5
                    , p_saty = 0
                    , tired_coef = 0 ) :
    '''
    number_random_walker : the number of random walkers ===> integer
    number_step : the number of steps that each random walker go ===>integer
    length_step : the length of each step
    p_forward : probability of going forward
    p_backward : probability of going backward
    p_stay : probability of going stay
    tired_coef : for tired random walker
    
    '''
    m = np.zeros([number_step , number_random_walker ])
    for i in range(number_step-1):
            m[i+1] = m[i] + length_step * np.random.choice(np.arange(-1,2)
                                             , size = (1,number_random_walker) 
                                             , p=[p_backward,p_saty,p_forward])*np.exp(-i*tired_coef)
    m = m.T
    return  m


def loss ( y , x ): 
    loss = np.sqrt(np.sum((x - y * length_step)**2)) /len(x)
    return loss

def dup_rows(a, indx, num_dups=1):
    return np.insert(a,[indx+1]*num_dups,a[indx],axis=0)

def mean(x):
    mean = np.array([np.mean(x, axis = 0)])
    return mean

def variance(x):
    k = mean(x)
    h = dup_rows(k, indx=0, num_dups=number_random_walker-1)
    variance = np.array([np.mean((x-h)**2, axis = 0)])
    b1 , b2 = variance.shape
    for i in range(b2):
        variance[0][i] = np.sqrt(float(variance[0][i]))
    return variance


# # Calculations

# In[4]:


get_ipython().run_cell_magic('time', '', 'm = random_walkers ( number_random_walker = number_random_walker\n                    , number_step = number_step\n                    , length_step = length_step)')


# In[5]:


##calculate mean , variance and last location of random walkers 
mean_t = mean(m)[0]
variance_t = variance(m)[0]
random_walker_loc = m[:,-1]


# # Plot

# plotting histogram of last location of random walkers 

# In[6]:


plt.hist(random_walker_loc,50)
plt.savefig('show_var_n_4.png')


# plotting Graph of mean of location

# In[7]:


a = np.linspace( 0 , number_step , number_step )
fig = plt.figure()
plt.plot(a, mean_t , 'g') # plotting t, b separately 
plt.xlabel('Number of steps', fontsize = 15)
plt.ylabel('mean of Location', fontsize =15)
plt.title('Graph of mean of location')
plt.savefig('mean of Location_n_4.png')
plt.show()


# plotting Graph of variance of location

# In[8]:


n = np.linspace( 0 , number_step , number_step )
for i in range(number_step):
    n[i] = np.sqrt(float(n[i]))
fig = plt.figure()
plt.plot(n, variance_t , 'g') # plotting t, b separately 
plt.xlabel('sqrt(Number of steps)', fontsize = 15)
plt.ylabel('variance of location', fontsize =15)
plt.title('Graph of variance of location', fontsize =15)
plt.show()


# # Machine learning

# I use machine learning to get the slope

# In[9]:


from sklearn.linear_model import LinearRegression
## The linear model
random_walk_lin = LinearRegression()
random_walk_lin.fit( np.array(n).reshape(-1,1) , np.array(variance_t))

fig = plt.figure()
ax = fig.add_subplot(111, xlabel='sqrt(Number of steps)', ylabel='variance of location', title='Graph of variance of location')

ax.plot(np.array(n).reshape(-1,1), random_walk_lin.predict( np.array(n).reshape(-1,1) ) ,label='Linear Regression' )
ax.scatter(np.array(n).reshape(-1,1) , variance_t , color='r',label='Data' )
ax.legend(loc=0)
plt.savefig('variance of Location_n_4.png')
plt.show()


# In[10]:


print('The slope of regression line :  s = {}'.format(random_walk_lin.coef_))
print('The loss of regression line from correct formula :  loss = {}'.format(loss(n , variance_t)))
print('The variance of end loction : variance = {}' . format((variance_t[-1])))


# In[35]:


for i in range(number_random_walker):
    x = m[i]
    plt.plot(a, x ,'g-')

plt.xlabel('number of step')
# Set the y axis label of the current axis.
plt.ylabel('location')
# Set a title of the current axes.
plt.title(' Random Walkers')
#Save image
plt.savefig('curly_n_4.png')
# Display a figure.
plt.show()


# ### 3D simulation

# https://matplotlib.org/gallery/animation/simple_3danim.html

# In[3]:


import numpy as np
import matplotlib.pyplot as plt
import mpl_toolkits.mplot3d.axes3d as p3
import matplotlib.animation as animation
from IPython.display import HTML
animation.rcParams['animation.embed_limit'] = 2**128


# In[13]:


number_random_walker = int (input("number_random_walker?  "))
number_step = int (input("number_step?  "))
length_step = float(input("length_step?  "))


# In[14]:


m_x = random_walkers ( number_random_walker = number_random_walker
                    , number_step = number_step
                    , length_step = length_step)
m_y = random_walkers ( number_random_walker = number_random_walker
                    , number_step = number_step
                    , length_step = length_step)
m_z = random_walkers ( number_random_walker = number_random_walker
                    , number_step = number_step
                    , length_step = length_step)


# In[15]:


data = m = np.zeros([number_random_walker , 3 , number_step])
for i in range (number_random_walker):
    data[i][0] = m_x[i]
    data[i][1] = m_y[i]
    data[i][2] = m_z[i]


# In[16]:


variance_t_x = variance(m_x)[0][-1]
variance_t_y = variance(m_y)[0][-1]
variance_t_z = variance(m_z)[0][-1]
binary = (variance_t_x + variance_t_y + variance_t_z)
if binary < 1 :
    binary = binary + 1


# In[17]:


def update_lines(num, data, lines):
    for line , data in zip(lines, data):
        # NOTE: there is no .set_data() for 3 dim data...
        line.set_data(data[0:2, :num])
        line.set_3d_properties(data[2, :num])
    return lines

# Attaching 3D axis to the figure
fig = plt.figure()
ax = p3.Axes3D(fig)

# Fifty lines of random 3-D lines
data = data

# Creating fifty line objects.
# NOTE: Can't pass empty arrays into 3d version of plot()
lines = [ax.plot(dat[0, 0:1], dat[1, 0:1], dat[2, 0:1])[0] for dat in data]

# Setting the axes properties
ax.set_xlim3d([-binary, binary])
ax.set_xlabel('X')

ax.set_ylim3d([-binary, binary])
ax.set_ylabel('Y')

ax.set_zlim3d([-binary, binary])
ax.set_zlabel('Z')

ax.set_title('3D Simulation')

# Creating the Animation object
ani = animation.FuncAnimation(fig, update_lines,  number_step , fargs=(data, lines),
                                   interval=50, blit=False)


HTML(ani.to_jshtml())


# In[ ]:




